import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { timer } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { Exchange } from '../../interfaces/exchange';

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {

  constructor(private http: HttpClient) { }

  latest(from, to) {
    const source = timer(0, 10000);
    return source.pipe(
      switchMap(_ => this.http.get(`https://api.exchangeratesapi.io/latest?base=${from}&symbols=${to}`)),
      map((value: Exchange) => value.rates[to])
    );
  }
}
