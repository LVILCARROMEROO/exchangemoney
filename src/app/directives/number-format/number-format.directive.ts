import {Directive, ElementRef, HostListener, Input} from '@angular/core';
import {OwnCurrencyPipe} from '../../pipes/own-currency/own-currency.pipe';

@Directive({
  selector: '[appNumberFormat]',
  providers: [OwnCurrencyPipe]
})
export class NumberFormatDirective {
  private el: HTMLInputElement;
  @Input('currencySymbol') symbol: string;
  @Input('currencyDecimals') decimals: number;

  constructor(private elementRef: ElementRef, private currency: OwnCurrencyPipe) {
    this.el = this.elementRef.nativeElement;
  }

  @HostListener('input', ['$event.target.value'])
  onInput(value) {
    this.el.value = value.replace(/[^0-9.]/g, '');
  }

  @HostListener('blur', ['$event.target.value'])
  onBlur(value) {
    this.el.value = this.currency.transform(value, this.symbol, this.decimals);
  }

  @HostListener('keyup', ['$event.keyCode', '$event.target.value'])
  onKeyUp(keyCode, value) {
    if (keyCode === 13){
      this.el.value = this.currency.transform(value, this.symbol, this.decimals);
    }
  }

  @HostListener('focus', ['$event.target.value'])
  onFocus(value) {
    this.el.value = this.currency.parse(value, this.symbol, this.decimals);
  }

}
