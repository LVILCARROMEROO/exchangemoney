import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterSectionItemComponent } from './footer-section-item.component';

describe('FooterSectionItemComponent', () => {
  let component: FooterSectionItemComponent;
  let fixture: ComponentFixture<FooterSectionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterSectionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterSectionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
