import { Pipe, PipeTransform } from '@angular/core';

const DEFAULT_DECIMALS = '0000';
const CURRENCY_SYMBOL = {
  USD: '$',
  EUR: '€'
};
const getSymbol = (symbol, integer?) => {
  if (symbol && Object(CURRENCY_SYMBOL).hasOwnProperty(symbol)) {
    const currentSymbol = CURRENCY_SYMBOL[symbol];
    if (integer && integer.indexOf(currentSymbol) > -1) { return ''; }
    return CURRENCY_SYMBOL[symbol];
  }
  return '';
};

@Pipe({
  name: 'ownCurrency'
})
export class OwnCurrencyPipe implements PipeTransform {

  private DECIMAL_SEPARATOR: string = String('.');
  private THOUSANDS_SEPARATOR: string = String(',');

  transform(value: number | string, symbol = 'USD', fractionSize: number = 2): string {
    if (!value) { return null; }
    let [ integer, fraction = '' ] = value.toString().split(this.DECIMAL_SEPARATOR);

    fraction = fractionSize > 0
      ? this.DECIMAL_SEPARATOR + (fraction + DEFAULT_DECIMALS).substring(0, fractionSize)
      : '';

    return getSymbol(symbol,integer) + integer + fraction;
  }

  parse(value: string, symbol = 'USD', fractionSize: number = 2): string {
    if (!value) { return null; }
    let [ integer, fraction = '' ] = value.split(this.DECIMAL_SEPARATOR);

    integer = integer.replace(getSymbol(symbol), '');
    integer = integer.replace(new RegExp(this.THOUSANDS_SEPARATOR, 'g'), '');

    fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
      ? this.DECIMAL_SEPARATOR + (fraction + DEFAULT_DECIMALS).substring(0, fractionSize)
      : '';

    return integer + fraction;
  }


}
