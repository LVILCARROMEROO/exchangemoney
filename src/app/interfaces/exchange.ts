export interface Exchange {
  base: String;
  date: String;
  rates: Object;
}
