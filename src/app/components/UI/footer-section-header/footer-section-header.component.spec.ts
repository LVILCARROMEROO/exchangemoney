import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterSectionHeaderComponent } from './footer-section-header.component';

describe('FooterSectionHeaderComponent', () => {
  let component: FooterSectionHeaderComponent;
  let fixture: ComponentFixture<FooterSectionHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterSectionHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterSectionHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
