import {Component, OnInit} from '@angular/core';
import {ExchangeService} from './services/exchange/exchange.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  from: String = 'USD';
  to: String = 'EUR';
  decimals: 4;
  converted: number = null;
  exchangeValue: number;
  exhangeForm: FormGroup;
  constructor(
    private exchangeService: ExchangeService,
    public fb: FormBuilder
  ) {
    this.exhangeForm = this.fb.group({
        toConvert: ['', Validators.required]
    });

    this.exhangeForm.valueChanges.subscribe(val => {
        if (!val.toConvert) { this.converted = null; }
    });
  }

  ngOnInit() {
    this.exchangeService.latest(this.from, this.to)
      .subscribe(
        (result: number) => this.exchangeValue = result,
        (error: any) => console.log(error)
      );
  }

  convert() {
    const toConvert = +this.exhangeForm.value.toConvert;
    this.converted = toConvert * this.exchangeValue;
  }

  isValid() {
    return (this.exhangeForm.valid && +this.exhangeForm.value.toConvert);
  }
}
