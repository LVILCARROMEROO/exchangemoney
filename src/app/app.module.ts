import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NumberFormatDirective } from './directives/number-format/number-format.directive';
import { OwnCurrencyPipe } from './pipes/own-currency/own-currency.pipe';
import { HeaderComponent } from './components/UI/header/header.component';
import { FooterComponent } from './components/UI/footer/footer.component';
import { AvatarComponent } from './components/UI/avatar/avatar.component';
import { NavComponent } from './components/UI/nav/nav.component';
import { NavItemComponent } from './components/UI/nav-item/nav-item.component';
import { SubsectionComponent } from './components/UI/subsection/subsection.component';
import { SectionComponent } from './components/UI/section/section.component';
import { FooterSectionComponent } from './components/UI/footer-section/footer-section.component';
import { FooterSectionHeaderComponent } from './components/UI/footer-section-header/footer-section-header.component';
import { FooterSectionItemComponent } from './components/UI/footer-section-item/footer-section-item.component';

@NgModule({
  declarations: [
    AppComponent,
    NumberFormatDirective,
    OwnCurrencyPipe,
    HeaderComponent,
    FooterComponent,
    AvatarComponent,
    NavComponent,
    NavItemComponent,
    SubsectionComponent,
    SectionComponent,
    FooterSectionComponent,
    FooterSectionHeaderComponent,
    FooterSectionItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
